#
#  Be sure to run `pod spec lint ACExtensions.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name                  = "ACExtensions"
  spec.version               = "0.0.3"
  spec.summary               = "Useful Swift Extensions"
  spec.description           = <<-DESC
	"Useful Swift Extensions"
                   DESC
  spec.homepage              = "https://gitlab.com/AlexandreCools/ac-extensions"
  #spec.screenshots          = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"
  spec.license               = { :type => "MIT", :file => "LICENSE" }
  spec.author                = { "Alexandre Cools" => "hello@alexandrecools.com" }
  spec.platform              = :ios
  spec.ios.deployment_target = "12.0"
  spec.swift_versions        = "5.0"
  spec.source                = { :git => "https://gitlab.com/AlexandreCools/ac-extensions.git", :tag => "#{spec.version}" }
  spec.source_files          = "Classes", "ACExtensions/ACExtensions/Classes/**/*"

end
