//
//  ACExtensions.h
//  ACExtensions
//
//  Created by Alexandre Cools on 05/01/2020.
//  Copyright © 2020 Alexandre Cools. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for ACExtensions.
FOUNDATION_EXPORT double ACExtensionsVersionNumber;

//! Project version string for ACExtensions.
FOUNDATION_EXPORT const unsigned char ACExtensionsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ACExtensions/PublicHeader.h>


