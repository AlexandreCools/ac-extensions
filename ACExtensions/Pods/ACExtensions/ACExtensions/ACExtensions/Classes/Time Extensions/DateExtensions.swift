//
//  DateExtensions.swift
//  ACExtensions
//
//  Created by Alexandre Cools on 05/01/2020.
//  Copyright © 2020 Alexandre Cools. All rights reserved.
//

import Foundation

public extension Date {
    
    static var calendar: Calendar {
        var calendar = Calendar(identifier: .gregorian)
        calendar.firstWeekday = Day.monday
        return calendar
    }
    
}

public extension Date {
    
    var isToday: Bool {
        Date.calendar.isDate(self, equalTo: Date(), toGranularity: .day)
    }
    
    var isInCurrentWeek: Bool {
        Date.calendar.isDate(self, equalTo: Date(), toGranularity: .weekOfMonth)
    }
    
    var isInCurrentMonth: Bool {
        Date.calendar.isDate(self, equalTo: Date(), toGranularity: .month)
    }
    
    var isInCurrentYear: Bool {
        Date.calendar.isDate(self, equalTo: Date(), toGranularity: .year)
    }
    
    var monthAndYear: String {
        let year = self.year
        
        let formatter = DateFormatter()
        formatter.dateFormat = "LLLL"
        let month = formatter.string(from: self)
        
        return "\(month) \(year)"
    }
    
    var year: String {
        let calendar = Date.calendar
        let year = calendar.component(.year, from: self)
        return "\(year)"
    }
    
    func isInDay(_ day: Date) -> Bool {
        Date.calendar.isDate(self, equalTo: day, toGranularity: .day)
    }
    
    func isInWeekOf(day: Date) -> Bool {
        Date.calendar.isDate(self, equalTo: day, toGranularity: .weekOfYear)
    }
    
    func isInMonthOf(day: Date) -> Bool {
        Date.calendar.isDate(self, equalTo: day, toGranularity: .month)
    }
    
    func isInYearOf(day: Date) -> Bool {
        Date.calendar.isDate(self, equalTo: day, toGranularity: .year)
    }
    
}

public extension Date {
    
    var startOfWeek: Date? {
        let calendar = Date.calendar
        guard let sunday = calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return calendar.date(byAdding: .day, value: 0, to: sunday)
    }

    var endOfWeek: Date? {
        let calendar = Date.calendar
        guard let sunday = calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return calendar.date(byAdding: .day, value: 6, to: sunday)
    }
    
    var startOfMonth: Date? {
        let calendar = Date.calendar
        return calendar.date(from: calendar.dateComponents([.year, .month], from: self))
    }
    
    var endOfMonth: Date? {
        guard let startOfMonth = self.startOfMonth else { return nil }
        return Date.calendar.date(byAdding: DateComponents(month: 1, day: -1), to: startOfMonth)
    }
    
}

public extension Date {
    
    var longDescription: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        return dateFormatter.string(from: self)
    }
    
    var shortDescription: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        return dateFormatter.string(from: self)
    }
    
}

public extension Date {
    
    struct Day {
        static let sunday: Int = 1
        static let monday: Int = 2
        static let tuesday: Int = 3
        static let wednesday: Int = 4
        static let thursday: Int = 5
        static let friday: Int = 6
        static let saturday: Int = 7
    }
    
}
