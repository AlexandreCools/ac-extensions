# AC Extensions

## Usage

ACExtensions is a set of Swift language extensions.

If you want use ACExtensions from a Swift file, add the following line to your Swift file:
```swift
import LBExtensions
```

## Requirements

ACExtensions is designed to work on iOS 12+ and is written in Swift.

## Installation

ACExtensions is available through CocoaPods.

In your Podfile:

```ruby
pod 'ACExtensions'
```

## Available Extensions

### Time Extensions

#### DateExtensions

```swift
static var today: Date

var isToday: Bool
var isTomorrow: Bool
var isInFuture: Bool
var isInPast: Bool
var isInCurrentWeek: Bool
var isInCurrentMonth: Bool
var isInCurrentYear: Bool
var monthAndYear: String
var year: String

var startOfWeek: Date?
var endOfWeek: Date?
var startOfMonth: Date?
var endOfMonth: Date?

var longDescription: String
var shortDescription: String

func isInDay(_ day: Date) -> Bool
func isInWeekOf(day: Date) -> Bool
func isInMonthOf(day: Date) -> Bool
func isInYearOf(day: Date) -> Bool
func isMoreRecentOrEqual(than date: Date?) -> Bool
```

## Author

Alexandre Cools - hello@alexandrecools.com